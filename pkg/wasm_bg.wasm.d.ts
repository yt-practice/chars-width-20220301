/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function getWidth(a: number, b: number): number;
export function getWidthCjk(a: number, b: number): number;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
