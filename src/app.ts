import { append } from './append'
import {
	getSize,
	getTextWidth,
	getWidth2,
	getWidth3,
	getWidthCjk,
	parse,
	parse3,
} from './get-width'

const icon = (node: Node, src: string) => {
	const img = document.createElement('img')
	img.src = src
	img.style.width = '1em'
	img.style.height = '1em'
	node.appendChild(img)
}

const mount = (el: Element) => {
	const unmount = () => {
		el.innerHTML = ''
	}
	unmount()
	const inputWrap = append(el, 'div')
	const input = append(inputWrap, 'textarea')
	input.style.width = '50%'
	if (
		!(input instanceof HTMLInputElement) &&
		!(input instanceof HTMLTextAreaElement)
	)
		throw new Error('未知のエラー')

	input.placeholder = '計測したい文字列を入力'
	input.value = [
		'たとえば、 👍🏻 は 2 文字で 👨‍👩‍👧‍👦 は 7 文字と',
		'ずべ゙での゙文゙字゙に゙濁゙゙点゙を゙づげよ゙ゔ',
		'すべての文字に濁点をつけよう',
		'abcdef'.repeat(6),
		'ﾎｹﾞﾎｹﾞﾎｹﾞホゲホゲ',
		'ÀÁÂÃÄÅ',
		'ÀÁÂÃÄÅᰂᰃᰄᰅᰆᰇᰈᰉᰊᰋᰌᰍᰎ',
		'ﾋ゙ﾟ゙ﾖ゙ﾋ゙ﾟ゙ﾖ゙ﾋ゙ﾟ゙ﾖ゙',
		'',
		'ずべ゙での゙文゙字゙に゙濁゙゙点゙を゙づげよ゙ゔ'.repeat(10),
		'すべての文字に濁点をつけよう'.repeat(10),
	].join('\n')

	const outputWrap = append(append(el, 'div'), 'pre')

	append(document.head, 'style').innerHTML = `
	@import url("https://fonts.googleapis.com/css?family=Noto+Sans+JP&display=swap");
	@font-face {
		font-family: "hankaku";
		src: local("ＭＳ ゴシック"), local("Osaka-mono"), local("Osaka-等幅"), local(monospace);
	}
	@font-face {
		font-family: "hankaku_p";
		src: local("ＭＳ ゴシック"), local("Osaka-mono"), local("Osaka-等幅"), local(monospace);
		unicode-range: U+0x0-0x7f;
	}
	@font-face {
		font-family: "hankaku2_p";
		src: local("ＭＳ 明朝"), local("ＭＳ ゴシック"), local("Osaka-等幅"), local("Osaka-mono"), local(monospace);
		unicode-range: U+0x0-0x7f;
	}
	@font-face {
		font-family: "zenkaku";
		src: local("メイリオ"), local(Meiryo), local("Osaka-mono"), local("Osaka-等幅"), local("ＭＳ ゴシック"), local(monospace);
	}
	@font-face {
		font-family: "zenkaku2";
		src: local("Osaka-等幅"), local("Osaka-mono"), local("ＭＳ 明朝"), local("メイリオ"), local(Meiryo), local("ＭＳ ゴシック"), local(monospace);
	}
	`

	const initOutput = () => {
		const out = append(outputWrap, 'div')
		out.style.padding = '5px'

		const texts1: HTMLElement[] = []

		const d1 = append(out, 'div', '1234567890'.repeat(5))
		d1.style.font = `18px "hankaku_p", monospace`
		d1.style.lineHeight = '1em'
		append(d1, 'br')
		const text1 = append(d1, 'span')
		append(d1, 'br')
		texts1.push(append(d1, 'span'))

		const d2 = append(out, 'div', '1234567890'.repeat(5))
		d2.style.font = `18px "hankaku_p", zenkaku, monospace`
		d2.style.lineHeight = '1em'
		append(d2, 'br')
		texts1.push(append(d2, 'span'))

		const d3 = append(out, 'div', '1234567890'.repeat(5))
		d3.style.font = `18px "hankaku2_p", zenkaku2, monospace`
		d3.style.lineHeight = '1em'
		append(d3, 'br')
		texts1.push(append(d3, 'span'))

		const d4 = append(out, 'div', '1234567890'.repeat(5))
		d4.style.font = `18px "hankaku_p", zenkaku, monospace`
		d4.style.lineHeight = '1em'
		const texts2 = [
			(append(d4, 'br'), append(d4, 'span')),
			(append(d4, 'br'), append(d4, 'span')),
			(append(d4, 'br'), append(d4, 'span')),
			(append(d4, 'br'), append(d4, 'span')),
		] as const

		const outs = [
			append(out, 'output'),
			(append(out, 'br'), append(out, 'output')),
			(append(out, 'br'), append(out, 'output')),
			(append(out, 'br'), append(out, 'output')),
		] as const
		return { text1, texts1, texts2, outs }
	}

	const writeText1 = (node: Node, str: string) => {
		for (const r of parse(str))
			if ('string' === typeof r) append(node, 'span', r)
			else icon(node, r.url)
	}

	const hankaku = 'hankaku' as const
	const zenkaku = 'zenkaku' as const

	const writeText2 = (node: Node, str: string) => {
		for (const r of parse(str))
			if ('string' === typeof r) {
				const u = r.match(/./giu)
				if (u) {
					let last = null as null | { isASCII: boolean; el: HTMLElement }
					for (const m of u) {
						const isASCII = (m.codePointAt(0) || 0) <= 0x7f
						if (last?.isASCII === isASCII) {
							last.el.innerText += m
						} else {
							const el = append(node, 'span', m)
							el.style.fontFamily = isASCII ? hankaku : zenkaku
							last = { el, isASCII }
						}
					}
				}
			} else icon(node, r.url)
	}

	const writeText3 = (node: Node, str: string) => {
		for (const r of parse3(str, s => s < 2))
			switch (r.type) {
				case 'emoji':
					icon(node, r.url)
					break
				case 'hankaku':
					append(node, 'span', r.value).style.fontFamily = hankaku
					break
				case 'zenkaku':
					append(node, 'span', r.value).style.fontFamily = zenkaku
					break
			}
	}

	const writeText4 = (node: Node, str: string) => {
		for (const r of parse3(str, s => 1 === s))
			switch (r.type) {
				case 'emoji':
					icon(node, r.url)
					break
				case 'hankaku':
					append(node, 'span', r.value).style.fontFamily = hankaku
					break
				case 'zenkaku':
					append(node, 'span', r.value).style.fontFamily = zenkaku
					break
			}
	}

	const writeText5 = (node: Node, str: string) => {
		for (const r of parse3(str, (_s, c) => !c.trim()))
			switch (r.type) {
				case 'emoji':
					icon(node, r.url)
					break
				case 'hankaku':
					append(node, 'span', r.value).style.fontFamily = hankaku
					break
				case 'zenkaku':
					append(node, 'span', r.value).style.fontFamily = 'Noto Sans JP'
					break
			}
	}

	const render = () => {
		outputWrap.innerHTML = ''
		const strs = input.value
			.replace(/\r\n/giu, '\n')
			.replace(/\r/giu, '\n')
			.split('\n')
		for (const str of strs) {
			if (!str.trim()) continue
			console.log({
				str,
				p: str
					.match(/./giu)
					?.map(t => [
						t,
						t.codePointAt(0),
						(t.codePointAt(0) || 0) <= 0x7f,
						getSize(t, true),
					]),
			})
			const o = initOutput()
			o.text1.innerText = str
			for (const t of o.texts1) writeText1(t, str)
			writeText2(o.texts2[0], str)
			writeText3(o.texts2[1], str)
			writeText4(o.texts2[2], str)
			writeText5(o.texts2[3], str)
			o.outs[0].innerText = `width1: ${getWidthCjk(str)} (Rust)`
			o.outs[1].innerText = `width2: ${getWidth2(str)} (JavaScript)`
			o.outs[2].innerText = `width3: ${getTextWidth(str)} (canvas)`
			o.outs[3].innerText = `width4: ${getWidth3(str)} (JavaScript 2)`
		}
	}
	input.addEventListener('input', render)
	render()

	return unmount
}

export const startApp = () => {
	const appEl = append(document.body, 'div')
	append(document.body, 'hr')
	mount(appEl)
}
