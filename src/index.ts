import { startApp } from './app'
import { init } from './get-width'
import { main } from './main'

Promise.resolve()
	.then(async () => {
		await init()
		startApp()
		await main()
	})
	.catch(x => {
		console.error(x)
	})
