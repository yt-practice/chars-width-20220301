import { append } from './append'
import { getTextWidth, getWidth2, getWidthCjk, parse } from './get-width'

const code = (t: string) => {
	const code = document.createElement('pre')
	code.style.fontFamily = `Osaka-mono, "Osaka-等幅", "ＭＳ ゴシック", monospace`
	code.textContent = t
	return code
}
const icon = (src: string) => {
	const img = document.createElement('img')
	img.src = src
	img.style.width = '1em'
	img.style.height = '1em'
	return img
}

const test = (str: string) => {
	const res = parse(str)
	console.log(res)
	const wrap = append(document.body, 'div')
	append(wrap, 'hr')
	const forString = (line: HTMLElement, r: string) => {
		const p = document.createElement('span')
		line.appendChild(p)
		const info = {
			text: r,
			length: r.length,
			width: getWidthCjk(r),
			width2: getWidth2(r),
			width3: getTextWidth(r),
		}
		console.log(info)
		line.appendChild(code(JSON.stringify(info, null, 2)))
	}
	const line = append(wrap, 'div')
	forString(line, str)
	append(wrap, 'hr')
	for (const r of res) {
		const line = append(wrap, 'div')
		if ('string' === typeof r) {
			forString(line, r)
		} else {
			line.appendChild(icon(r.url))
			line.appendChild(code(JSON.stringify(r, null, 2)))
		}
	}
	append(wrap, 'hr')
}

export const main = async () => {
	document.body.style.font = `14px Osaka-mono, "Osaka-等幅", "ＭＳ ゴシック", monospace`
	document.body.style.whiteSpace = 'pre-wrap'
	console.log('main')
	test('たとえば、 👍🏻 は 2 文字で 👨‍👩‍👧‍👦 は 7 文字と')
	test('ずべ゙での゙文゙字゙に゙濁゙゙点゙を゙づげよ゙ゔ')
	test('string(25) "👨‍👩‍👦‍👧"')

	const tag = (tagName: 'p' | 'span', text: string) => {
		const d = document.createElement(tagName)
		d.style.margin = '0'
		d.style.padding = '0'
		d.textContent = text
		return d
	}
	const line = (tagName: 'p' | 'span') => {
		const d = document.createElement(tagName)
		d.style.margin = '0'
		d.style.padding = '0'
		const self = {
			span: (t: string) => {
				d.appendChild(tag('span', t))
				return self
			},
			nodes: (nodes: readonly Node[]) => {
				for (const t of nodes) d.appendChild(t)
				return self
			},
			get: () => d,
		} as const
		return self
	}
	const div = (node: Node) => {
		const d = append(node, 'div')
		d.style.padding = '20px'
		d.style.marginBottom = '100px'
		d.style.font = `18px Osaka-mono, "Osaka-等幅", "ＭＳ ゴシック", monospace`
		d.style.lineHeight = '1em'
		d.style.fontFeatureSettings = '"palt" 0, "tnum", "liga" 0'
		const self = {
			line: (t: string) => {
				d.appendChild(tag('p', t))
				return self
			},
			node: (t: Node) => {
				d.appendChild(t)
				return self
			},
		} as const
		return self
	}
	const res = parse('たとえば、 👍🏻 は 2 文字で 👨‍👩‍👧‍👦 は 7 文字と')

	div(document.body)
		.line('たとえば、 👍🏻 は 2 文字で 👨‍👩‍👧‍👦 は 7 文字と')
		.node(
			line('p')
				.nodes(
					res.map(r => {
						if ('string' === typeof r) return tag('span', r)
						return icon(r.url)
					}),
				)
				.get(),
		)
		.line('1234567890123456789012345678901234567890')
		.line(
			Array.from({ length: 26 }, (_, i) =>
				String.fromCharCode('a'.charCodeAt(0) + i),
			).join(''),
		)
}
