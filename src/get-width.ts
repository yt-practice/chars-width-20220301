import easta from 'easta'
import * as parser from 'twemoji-parser'

import * as pkg from '../pkg'

import { UnicodeData } from './gen/UnicodeData'

const canvas = document.createElement('canvas')
const context = canvas.getContext('2d')
if (!context) throw new Error('wtf')
context.font = `14px Osaka-mono, "Osaka-等幅", "ＭＳ ゴシック", monospace`
/**
 * canvas による計測
 */
export const getTextWidth = (text: string) => {
	const metrics = context.measureText(text)
	return metrics.width / 7
}

/**
 * 文字列から絵文字を切り出す
 */
export const parse = (str: string) => {
	const list = parser.parse(str)
	const res: (string | parser.EmojiEntity)[] = []
	let last = 0
	for (const p of list) {
		const tmp = str.substring(last, p.indices[0])
		if (tmp) res.push(tmp)
		last = p.indices[1]
		res.push(p)
	}
	const tmp = str.substring(last)
	if (tmp) res.push(tmp)
	return res
}

export const getSize = (c: string, isCjk: boolean) => {
	const w = easta(c)
	const k = c.codePointAt(0)
	if (!k) return 0
	const w2 = UnicodeData[k as unknown as keyof typeof UnicodeData]
	if ('Me' === w2 || 'Mn' === w2 || 'Cf' === w2 || 'Cc' === w2) return 0
	if ('Cs' === w2) return 2
	if ('F' === w || 'W' === w) return 2
	if ('A' === w) return isCjk ? 2 : 1
	if ('N' === w && ('Zl' === w2 || 'Zp' === w2)) return 1
	return 1
}

/**
 * js による計測
 */
export const getWidth2 = (str: string): number => {
	const list = str.match(/./giu)
	if (!list) return 0
	return list.map((r): number => getSize(r, true)).reduce((q, w) => q + w, 0)
}

/**
 * js による計測 2
 */
export const getWidth3 = (str: string): number =>
	parse(str)
		.map(r => {
			if ('string' === typeof r) {
				const list = r.match(/./giu)
				if (!list) return 0
				return list
					.map((r): number => getSize(r, true))
					.reduce((q, w) => q + w, 0)
			}
			return 2
		})
		.reduce((q, w) => q + w, 0)

/**
 * 文字列から絵文字を切り出す
 */
export const parse3 = (
	str: string,
	getIsHan: (size: number, char: string) => boolean,
) => {
	const list = parser.parse(str)
	type Part =
		| Readonly<{ type: 'zenkaku'; size: 2; value: string }>
		| Readonly<{ type: 'hankaku'; size: 1; value: string }>
		| Readonly<{ type: 'emoji'; size: 2; url: string }>
	const res: Part[] = []
	let last = 0
	const push = (str: string) => {
		let last = null as null | {
			type: string
			isHan: boolean
			size: number
			value: string
		}
		const list = str ? str.match(/./giu) : null
		if (!list) return
		for (const m of list) {
			const s = getSize(m, true)
			const isHan = getIsHan(s, m)
			if (last?.isHan === isHan) {
				last.value += m
			} else {
				const type = isHan ? 'hankaku' : 'zenkaku'
				last = { type, value: m, size: s, isHan }
				res.push(last as Part)
			}
		}
	}
	for (const p of list) {
		push(str.substring(last, p.indices[0]))
		last = p.indices[1]
		res.push({ ...p, size: 2 })
	}
	push(str.substring(last))
	return res
}

/**
 * 初期化およびメモリのリセット
 *
 * おそらく wasm に渡した文字列のメモリからの削除をしてない？ので定期的にリセットする必要がありそう
 */
export const init = async () => {
	const wasm =
		'production' === process.env.NODE_ENV
			? '/chars-width-20220301/pkg/wasm_bg.wasm'
			: '/pkg/wasm_bg.wasm'
	await pkg.default(wasm)
}

const self = window as unknown as { [k: string]: unknown }
self.getWidth2 = getWidth2
self.getTextWidth = getTextWidth
self.getWidthCjk = pkg.getWidthCjk

/**
 * rust による計測
 */
export const getWidthCjk = (str: string) => pkg.getWidthCjk(str)

self.debugFn = () => {
	const map = new Map<string, number>()
	const hankaku: string[] = []
	const errors = Array.from({ length: 1114109 + 1 }, (_, k) => {
		const r = String.fromCodePoint(k)
		const w = easta(r)
		const w2 = UnicodeData[k as unknown as keyof typeof UnicodeData]
		if ('A' === w && 'Cf' === w2) return
		const rust = getWidthCjk(r)
		const js = getWidth2(r)
		if (1 === js && 0x7f < k) hankaku.push(r)
		const ok =
			rust === js ||
			(!w2 && 2 === rust && 1 === js) ||
			('N' === w && 'Lo' === w2 && 0 === rust && 1 === js) ||
			8232 === k ||
			8233 === k
		const hash = `${w}:${(w2 as string) ?? ''}:${ok ? 'ok' : 'er'}`
		if (!ok || 'N' === w) map.set(hash, (map.get(hash) ?? 0) + 1)
		if (!ok) {
			const canvas = getTextWidth(r)
			return { k, r, rust, js, canvas, w, w2 }
		}
	}).filter(<V>(v?: V): v is V => !!v)
	console.log(
		'map =',
		Array.from(map.entries()).sort((q, w) => q[0].localeCompare(w[0])),
	)
	console.log('errors =', errors)
	console.log('hankaku =', hankaku)
}
