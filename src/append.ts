export const append = (parent: Node, tagName: string, content?: string) => {
	const el = document.createElement(tagName)
	parent.appendChild(el)
	if (content) el.innerText = content
	return el
}
