
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

use wasm_bindgen::prelude::*;

#[wasm_bindgen(js_name = getWidth)]
pub fn get_width(str: &str) -> usize {
	use unicode_width::UnicodeWidthStr;
	UnicodeWidthStr::width(str)
}

#[wasm_bindgen(js_name = getWidthCjk)]
pub fn get_width_cjk(str: &str) -> usize {
	use unicode_width::UnicodeWidthStr;
	UnicodeWidthStr::width_cjk(str)
}
