import { fetch } from 'cross-fetch'

export const fetchUnicodeData = async () => {
	const fetchText = async (uri: string) =>
		await fetch(uri).then(async r => await r.text())
	const UnicodeData = await fetchText(
		'https://www.unicode.org/Public/UNIDATA/UnicodeData.txt',
	).then(t =>
		Object.fromEntries(
			t
				.split('\n')
				.map(t => t.trim().split(';'))
				.filter(t => t[0] && t[2])
				.map((t): [number, string] => [parseInt(t[0] ?? '', 16), t[2] ?? '']),
		),
	)
	return UnicodeData
}
