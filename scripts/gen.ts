import { writeFile } from 'fs/promises'
import { join } from 'path'

import { fetchUnicodeData } from './fetch-unicode-data'

export const gen = async () => {
	const UnicodeData = await fetchUnicodeData()
	await writeFile(
		join(__dirname, '../src/gen/UnicodeData.ts'),
		`export const UnicodeData = ${JSON.stringify(UnicodeData)} as const\n`,
	)
}

gen().catch(x => {
	console.error(x)
})
